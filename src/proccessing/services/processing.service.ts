import { RabbitSubscribe } from "@golevelup/nestjs-rabbitmq";
import { Inject, forwardRef } from "@nestjs/common";
import { ORDER_STATUS_EVENT_NAME, RABBIT_EXCHANGE } from "src/common/constants/rabbitmq";
import { EventBusService } from "src/eventbus/service/eventbus.service";

export class DemandService {
    constructor(
        @Inject(forwardRef(() => EventBusService))
        private readonly eventbus: EventBusService,
    ){
    }
    private async getRandomBoolean() {
        return Math.random() < 0.5;
    }
    private async getRandomNumber(): Promise<number> {
        const min = 2
        const max = 10
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private async delay(seconds) {
        return new Promise(resolve => setTimeout(resolve, seconds * 1000));
    }

    @RabbitSubscribe({
        exchange: RABBIT_EXCHANGE,
        routingKey: ORDER_STATUS_EVENT_NAME,
        queue: 'get-order-status',
        allowNonJsonMessages: true,
        queueOptions: {
            autoDelete: false,
            durable: true,
        },
    })
    public async returnResult(){
        const randomNumber = await this.getRandomNumber()
        await this.delay(randomNumber);
        return await this.getRandomBoolean()
    }
}