export class Meta {
  total_count: number;
  server_time: Date;
}

export class PaginatedResponse<T> {
  readonly meta: Meta;

  readonly items: T[];

  constructor(totalCount: number, items: T[]) {
    this.meta = {
      total_count: totalCount,
      server_time: new Date(),
    };
    this.items = items;
  }
}
