export class AppliedStatusDto {
    constructor(isApplied: boolean) {
      this.is_applied = isApplied;
    }
    is_applied: boolean;
  }
  