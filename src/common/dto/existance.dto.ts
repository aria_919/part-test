export class CheckExistDto {
    constructor(isExist: boolean) {
      this.is_exist = isExist;
    }
    is_exist: boolean;
  }
  