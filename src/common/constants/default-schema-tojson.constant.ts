export const DEFAULT_SCHEMA_TOJSON = {
  virtuals: true,
  versionKey: false,
  transform: function (_doc: any, ret: { _id: any; }) {
    delete ret._id;
  },
};
