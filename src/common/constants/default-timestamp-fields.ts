export const DEFAULT_TIMESTAMP_FIELDS = {
  createdAt: 'created_at',
  updatedAt: 'updated_at',
};