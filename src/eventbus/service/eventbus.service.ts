import { Injectable } from '@nestjs/common';
// import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { RABBIT_EXCHANGE, RABBIT_JOB_EXCHANGE } from '../../common/constants';

@Injectable()
export class EventBusService {
  constructor(
    // @InjectPinoLogger(EventBusService.name) private readonly logger: PinoLogger,
    private readonly amqpConnection: AmqpConnection,
  ) {}

  public async PublishEvent(topic: string, data: any) {
    try {
      await this.amqpConnection.publish(
        RABBIT_EXCHANGE,
        topic,
        data,
      );
      // this.logger.info({ topic, data }, 'event published successfully');
    } catch (err) {
      // this.logger.error({ err, topic, data }, 'publishing event failed');
    }
  }

  public async PublishJob(topic: string, data: any) {
    try {
      await this.amqpConnection.publish(
        RABBIT_JOB_EXCHANGE,
        topic,
        data,
      );
      // this.logger.info({ topic, data }, 'job published successfully');
    } catch (err) {
      // this.logger.error({ err, topic, data }, 'publishing job failed');
    }
  }

  // public SerializeEvent(data: any): any {
  //   return JSON.parse(data);  
  // }
}
