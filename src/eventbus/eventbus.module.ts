import { Module } from '@nestjs/common';
import { EventBusService } from './service';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

@Module({
  providers: [ EventBusService , AmqpConnection ],
  exports: [ EventBusService ],
})
export class EventBusModule {
}
