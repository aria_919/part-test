import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import mongoose from 'mongoose';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import { RABBIT_EXCHANGE, RABBIT_JOB_EXCHANGE } from './common/constants';
import { DemandModule } from './demand/demand.module';
// import { LoggerModule } from 'nestjs-pino';

@Module({
  imports: [
    DemandModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `.env${process.env.NODE_ENV ? '.' + process.env.NODE_ENV : ''}`,
      ignoreEnvFile: process.env.ENV_FILES !== 'true',
    }),
    // LoggerModule.forRootAsync({
    //   inject: [ConfigService],
    //   useFactory: (configService: ConfigService) => ({
    //     pinoHttp: {
    //       level: configService.get<string>('LOG_LEVEL', 'debug'),
    //       prettyPrint:
    //         configService.get<string>('LOG_PRETTY', 'false') === 'true',
    //     },
    //   }),
    // }),
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        mongoose.set('debug', config.get('MONGO_LOGGING', 'false') === 'true');
        return {
          uri: config.get<string>(
            'MONGODB_URI',
            'mongodb://test_user:test_password@127.0.0.1:27017/test_db?authSource=admin',
          ),
        };
      },
    }),
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          uri: config.get<string>(
            'RABBIT_DSN',
            'amqp://rabbitmq_user:rabbitmq_password@127.0.0.1:5672',
          ),
          exchanges: [
            { name: RABBIT_EXCHANGE, type: 'topic' },
            { name: RABBIT_JOB_EXCHANGE, type: 'topic' },
          ],
          durable: true,
          exclusive: false,
          prefetchCount: 1,
          registerHandlers: true,
        };
      },
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
  exports:[RabbitMQModule]
})
export class AppModule {}
