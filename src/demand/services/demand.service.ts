import { Inject, Injectable, NotFoundException, forwardRef } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Order, OrderDocument } from '../schemas/order.schema';
import { AppliedStatusDto } from 'src/common/dto/applied.dto';
import { Model } from 'mongoose';
import { CreateOrderDto } from '../dto/create-order.dto';
import { OrderStatusTypeEnum } from '../enums/order-status.enum';
// import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { EventBusService } from 'src/eventbus/service';
const EVENT_NAME = 'GET_BOOK_STATUS'
@Injectable()
export class DemandService {
  

  constructor(
    // @InjectPinoLogger(DemandService.name) private readonly logger: PinoLogger,
    @InjectModel(Order.name) private readonly OrderModel: Model<OrderDocument>,
    
    @Inject(forwardRef(() => EventBusService))
    private readonly eventbus: EventBusService,
    ) {}
  
  public async Create(createOrderDto : CreateOrderDto ) {
    await this.OrderModel.create(createOrderDto);
    const title = `درخواست کتاب ${createOrderDto.book} توسط کاربر به نام ${createOrderDto.user} ثبت شد.`

    const eventPayload = {
      title: title,
      book: createOrderDto.book,
      user: createOrderDto.user
    }
    // await this.eventbus.PublishEvent(EVENT_NAME, eventPayload);

    return { is_applied: true  , message:title};
  }

  public async FindOne(id: string) {
    const data = await this.OrderModel.findOne({ _id: id });
    DemandService.assertOnCurrencyNotFound(data);
    // TODO : add response interface
    // return new DemandInformationResponseDto(data); 
    return data
  }

  public async UpdateOrderStatus(id, status: OrderStatusTypeEnum) {
    const result = await this.OrderModel.updateOne(
      { _id: id },
      {
        $set: {
          status:status 
        },
      },
    );
    const isUpdated = result.modifiedCount > 0;
    return new AppliedStatusDto(isUpdated);
  }

  private static assertOnCurrencyNotFound(data: Partial<Order> | null) {
    if (!data) {
      throw new NotFoundException('demand not found');
    }
  }}
