import { Module, forwardRef } from '@nestjs/common';
import { Order, OrderSchema } from 'src/demand/schemas/order.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { DemandService } from './services/demand.service';
import { DemandController } from './controllers/demand.controller';
import { EventBusModule } from 'src/eventbus/eventbus.module';

@Module({
    imports:[
      
        MongooseModule.forFeature([
          { name: Order.name, schema: OrderSchema },
        ]),
        forwardRef(() =>  EventBusModule),
      ],
  providers: [ DemandService ],
  controllers: [DemandController],
  exports: [ DemandService ],
})
export class DemandModule {
}

