/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { DEFAULT_SCHEMA_TOJSON } from 'src/common/constants/default-schema-tojson.constant';
import { DEFAULT_TIMESTAMP_FIELDS } from 'src/common/constants/default-timestamp-fields';
import { OrderStatusTypeEnum } from '../enums/order-status.enum';
export type OrderDocument = Order & Document;

@Schema({
    autoCreate: true,
    autoIndex: true,
    timestamps: DEFAULT_TIMESTAMP_FIELDS,
  })
export class Order extends Document {

  @Prop({ required: true, trim: true  })
  user: string;

  @Prop({ required: true, trim: true  })
  book: string;

  @Prop({ type: Date })
  created_at?: Date;

  @Prop({ type: Date })
  updated_at?: Date;

  @Prop({
    required: true,
    enum: Object.keys(OrderStatusTypeEnum),
    default: OrderStatusTypeEnum.RESERVED,
  })
  status: OrderStatusTypeEnum;
}

export const OrderSchema = SchemaFactory.createForClass(Order);

OrderSchema.set('toJSON', DEFAULT_SCHEMA_TOJSON);