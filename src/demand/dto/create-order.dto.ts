import {
    IsDefined,
    IsString,
  } from "class-validator";
  
  export class CreateOrderDto {
  
    @IsDefined()
    @IsString()
    book: string;
  
    @IsDefined()
    @IsString()
    user?: string;
  }