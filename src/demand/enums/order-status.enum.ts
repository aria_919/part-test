export enum OrderStatusTypeEnum {
    RESERVED = 'RESERVED',
    SUCCESSFULL = 'SUCCESSFULL',
    REJECTED = 'REJECTED'
  }
  