import { Controller, Post, Body,  } from '@nestjs/common';
import { DemandService } from '../services/demand.service';
import { CreateOrderDto } from '../dto/create-order.dto';

@Controller('demand')
export class DemandController {
  constructor(private readonly demandService: DemandService) {}

  @Post()
  create(@Body() createOrderDto: CreateOrderDto) {
    return this.demandService.Create(createOrderDto);
  }

}
